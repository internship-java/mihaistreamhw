import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {

        ArrayList<String> list = new ArrayList<>();

        list.add("Unu");
        list.add("Casa");
        list.add("Masa");
        list.add("Mere");
        list.add("Pere");
        list.add("Pisica");
        list.add("Prasada");
        list.add("Tunis");
        list.add("Mumbai");
        list.add("vion");
        list.add("liopterh");
        list.add(" ");


        list.stream()
                .peek(e -> System.out.println(e))
                .sorted((val1, val2) -> val1.toLowerCase().compareTo(val2.toLowerCase()))
                .filter(e -> e.contains("e"))
                .peek(e -> System.out.println(e))
                .findFirst();

    }


}
